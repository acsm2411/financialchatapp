﻿namespace FinancialChatBot.Core.Services
{
    public interface IFileDownloadService
    {
        decimal GetStockQuoteFromWeb(string stockCode);
    }
}
