﻿namespace FinancialChatBot.Core.Services
{
    public interface IRabbitMQService
    {
        void Connect();
    }
}
