﻿using CsvHelper;
using FinancialChatApp.Common;
using FinancialChatBot.Core.Domain;
using FinancialChatBot.Core.Services;
using System;
using System.IO;
using System.Net;
using System.Text;

namespace FinancialChatBot.Services
{
    public class FileDownloadService : IFileDownloadService
    {
        public decimal GetStockQuoteFromWeb(string stockCode)
        {
            string tempDirectoryPath = $"{Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)}\\temp\\";
            string tempFileFullPath = string.Empty;

            try
            {
                DownloadCsvFile(stockCode, tempDirectoryPath, out tempFileFullPath);
                string quoteValueAsString = ReadStockQuoteFromCsvTempFile(tempFileFullPath);
                return IsValidQuoteValue(quoteValueAsString);
            }
            catch
            {
                throw;
            }
            finally
            {
                if (tempFileFullPath != string.Empty && File.Exists(tempFileFullPath))
                {
                    new FileInfo(tempFileFullPath).Delete();
                }
            }
        }

        private void DownloadCsvFile(string stockCode, string tempDirectoryPath, out string tempFileFullPath)
        {
            try
            {
                using (var client = new WebClient())
                {
                    if (!Directory.Exists(tempDirectoryPath))
                    {
                        Directory.CreateDirectory(tempDirectoryPath);
                    }

                    tempFileFullPath = $"{tempDirectoryPath}{stockCode}-{Guid.NewGuid()}.csv";

                    client.DownloadFile($"https://stooq.com/q/l/?s={stockCode}&f=sd2t2ohlcv&h&e=csv", tempFileFullPath);
                }
            }
            catch
            {
                throw new Exception(Literals.DOWNLOAD_ERROR);
            }
        }

        private string ReadStockQuoteFromCsvTempFile(string tempFileFullPath)
        {
            try
            {
                using (StreamReader reader = new StreamReader(tempFileFullPath, Encoding.Default))
                {
                    using (CsvReader csvReader = new CsvReader(reader, System.Globalization.CultureInfo.InvariantCulture))
                    {
                        return ReadDataFromCsv(csvReader);
                    }
                }
            }
            catch
            {
                throw new Exception(Literals.READING_ERROR);
            }
        }

        private string ReadDataFromCsv(CsvReader csvReader)
        {
            csvReader.Read();

            return csvReader.GetRecord<StockInfo>().Close;
        }

        private decimal IsValidQuoteValue(string quoteValueAsString)
        {
            if (quoteValueAsString != "N/D")
            {
                return decimal.Parse(quoteValueAsString, System.Globalization.CultureInfo.InvariantCulture);
            }
            else
            {
                throw new Exception(Literals.INVALID_STOCK_CODE);
            }
        }
    }
}
