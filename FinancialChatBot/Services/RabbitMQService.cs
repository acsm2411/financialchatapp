﻿using FinancialChatApp.Common;
using FinancialChatBot.Core.Services;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Text;

namespace FinancialChatBot.Services
{
    public class RabbitMQService : IRabbitMQService
    {
        private ConnectionFactory factory;
        private IConnection connection;
        private IModel channel;

        private readonly IFileDownloadService fileDownloadService;

        public RabbitMQService(IFileDownloadService fileDownloadService)
        {
            Configure();

            this.fileDownloadService = fileDownloadService;
        }

        public void Connect()
        {
            channel.QueueDeclare(queue: Literals.BOT_CHANNEL, durable: true, exclusive: false, autoDelete: false);

            EventingBasicConsumer consumer = new EventingBasicConsumer(channel);

            consumer.Received += (object model, BasicDeliverEventArgs ea) =>
            {

                string stockCode = Encoding.UTF8.GetString(ea.Body.ToArray());

                SendResponseBackToChat(stockCode);
            };

            channel.BasicConsume(queue: Literals.BOT_CHANNEL, autoAck: true, consumer: consumer);
        }

        public void SendMessage(string message)
        {
            var body = Encoding.UTF8.GetBytes(message);

            channel.BasicPublish(exchange: "",
                                routingKey: Literals.CHAT_CHANNEL,
                                basicProperties: null,
                                body: body);
        }

        private void Configure()
        {
            factory = new ConnectionFactory
            {
                HostName = Literals.HOST_NAME
            };
            connection = factory.CreateConnection();
            channel = connection.CreateModel();
        }

        private void SendResponseBackToChat(string stockCode)
        {
            try
            {
                decimal stockQuote = fileDownloadService.GetStockQuoteFromWeb(stockCode);

                string formattedMessage = $"{stockCode.ToUpper()} quote is ${stockQuote} per share";

                SendMessage(formattedMessage);
            }
            catch (Exception ex)
            {
                SendMessage(ex.Message);
            }
        }
    }
}
