﻿namespace FinancialChatApp.Common
{
    public static class Literals
    {
        public const string DOWNLOAD_ERROR = "An error has occurred while getting the data from the web, please try again";
        public const string READING_ERROR = "An error occurred while reading the data obtained from the web, please try again";
        public const string INVALID_STOCK_CODE = "The requested stock code is not valid or doesn't exist, please try with another code";
        public const string HOST_NAME = "localhost";
        public const string BOT_NAME = "Bot";
        public const string BOT_CHANNEL = "FinancialBotChannel";
        public const string CHAT_CHANNEL = "FinancialChatChannel";
    }
}
