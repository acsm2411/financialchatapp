﻿using System;

namespace FinancialChatApp.Common
{
    public static class ServerDateBuilder
    {
        public static string GetServerDateTime()
        {
            return DateTime.Now.ToString().Replace(". ", string.Empty).Replace(".", string.Empty);
        }

        public static string GetServerDateTime(DateTime timestamp)
        {
            return timestamp.ToString().Replace(". ", string.Empty).Replace(".", string.Empty);
        }
    }
}
