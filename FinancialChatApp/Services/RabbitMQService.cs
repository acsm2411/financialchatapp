﻿using FinancialChatApp.Common;
using FinancialChatApp.Core.Interfaces;
using FinancialChatApp.Hubs;
using Microsoft.AspNetCore.SignalR;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Text;

namespace FinancialChatApp.Services
{
    public class RabbitMQService : IRabbitMQService
    {
        private ConnectionFactory factory;
        private IConnection connection;
        private IModel channel;

        private readonly IServiceProvider serviceProvider;

        private const string HOST_NAME = "localhost";

        public RabbitMQService(IServiceProvider serviceProvider)
        {
            Configure();

            this.serviceProvider = serviceProvider;
        }

        public void Connect()
        {
            channel.QueueDeclare(queue: Literals.CHAT_CHANNEL, durable: true, exclusive: false, autoDelete: false);

            EventingBasicConsumer consumer = new EventingBasicConsumer(channel);

            consumer.Received += (object model, BasicDeliverEventArgs ea) =>
            {

                string receivedMessage = Encoding.UTF8.GetString(ea.Body.ToArray());

                var chatHub = (IHubContext<ChatHub>)serviceProvider.GetService(typeof(IHubContext<ChatHub>));
                chatHub.Clients.All.SendAsync("ReceiveMessage", Literals.BOT_NAME, receivedMessage
                    , ServerDateBuilder.GetServerDateTime());
            };

            channel.BasicConsume(queue: Literals.CHAT_CHANNEL, autoAck: true, consumer: consumer);
        }

        public void SendMessage(string message)
        {
            var body = Encoding.UTF8.GetBytes(message);

            channel.BasicPublish(exchange: "",
                                 routingKey: Literals.BOT_CHANNEL,
                                 basicProperties: null,
                                 body: body);
        }

        private void Configure()
        {
            factory = new ConnectionFactory
            {
                HostName = HOST_NAME
            };
            connection = factory.CreateConnection();
            channel = connection.CreateModel();
        }
    }
}
