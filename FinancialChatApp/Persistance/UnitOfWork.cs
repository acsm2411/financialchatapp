﻿using FinancialChatApp.Core;
using FinancialChatApp.Core.Repositories;

namespace FinancialChatApp.Persistance
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly FinancialChatAppDbContext context;

        public IMessageRepository MessageRepository { get; private set; }

        public UnitOfWork(FinancialChatAppDbContext context, IMessageRepository messageRepository)
        {
            this.context = context;
            this.MessageRepository = messageRepository;
        }

        public int Complete()
        {
            return context.SaveChanges();
        }

        public void Dispose()
        {
            context.Dispose();
        }
    }
}
