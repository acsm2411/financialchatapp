﻿using FinancialChatApp.Core.Domain;
using FinancialChatApp.Core.Repositories;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace FinancialChatApp.Persistance.Repositories
{
    public class MessageRepository : Repository<Message>, IMessageRepository
    {
        public FinancialChatAppDbContext FinancialChatAppContext => context as FinancialChatAppDbContext;

        public MessageRepository(FinancialChatAppDbContext context) : base(context) { }

        public List<Message> GetLastMessages()
        {
            List<Message> messages = FinancialChatAppContext.Messages.Include(m => m.User).OrderByDescending(m => m.Timestamp)
                .Take(50).ToList();

            messages.Reverse();

            return messages;
        }
    }
}
