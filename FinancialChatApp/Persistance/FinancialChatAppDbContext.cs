﻿using FinancialChatApp.Core.Domain;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace FinancialChatApp.Persistance
{
    public class FinancialChatAppDbContext : IdentityDbContext<User>
    {
        public DbSet<Message> Messages { get; set; }

        public FinancialChatAppDbContext(DbContextOptions<FinancialChatAppDbContext> options) : base(options)
        {

        }
    }
}
