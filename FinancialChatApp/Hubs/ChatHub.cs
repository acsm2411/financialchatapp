﻿using FinancialChatApp.Common;
using FinancialChatApp.Core;
using FinancialChatApp.Core.Domain;
using FinancialChatApp.Core.Interfaces;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Threading.Tasks;

namespace FinancialChatApp.Hubs
{
    public class ChatHub : Hub
    {
        private readonly IRabbitMQService rabbitMQService;
        private readonly IUnitOfWork unitOfWork;
        private readonly UserManager<User> userManager;

        public ChatHub(IRabbitMQService rabbitMQService,
            IUnitOfWork unitOfWork,
            UserManager<User> userManager)
        {
            this.rabbitMQService = rabbitMQService;
            this.unitOfWork = unitOfWork;
            this.userManager = userManager;
        }

        public async Task SendMessage(string user, string message)
        {
            await Clients.All.SendAsync("ReceiveMessage", user, message, ServerDateBuilder.GetServerDateTime());

            if (user != Literals.BOT_NAME && !message.Contains("/stock="))
            {
                await StoreNewMessage(user, message);
            }
        }

        public void SendExecutionSignalForFinancialBot(string stockCode)
        {
            try
            {
                rabbitMQService.SendMessage(stockCode);
            }
            catch
            {
                throw new HubException("Error while trying to execute the command, please try again");
            }
        }

        private async Task StoreNewMessage(string user, string body)
        {
            var message = new Message
            {
                User = await userManager.FindByNameAsync(user),
                Body = body,
                Timestamp = DateTime.Now
            };

            unitOfWork.MessageRepository.Add(message);
            unitOfWork.Complete();
        }
    }
}
