﻿using FinancialChatApp.Core.Domain;
using FinancialChatApp.Core.DTO;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace FinancialChatApp.Controllers
{
    public class LoginController : Controller
    {
        private readonly UserManager<User> userManager;
        private readonly SignInManager<User> signInManager;

        public LoginController(UserManager<User> userManager, SignInManager<User> signInManager)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Register()
        {
            return View();
        }

        public async Task<IActionResult> Logout()
        {
            await signInManager.SignOutAsync();
            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Index(LoginDTO login)
        {
            if (ModelState.IsValid)
            {
                User user = await userManager.FindByEmailAsync(login.Email);

                if (user != null)
                {
                    await signInManager.SignOutAsync();
                    Microsoft.AspNetCore.Identity.SignInResult result = await signInManager.PasswordSignInAsync(user, login.Password, false, false);

                    if (result.Succeeded)
                    {
                        return RedirectToAction("Index", "Chat");
                    }
                }

                ModelState.AddModelError(nameof(login.Email), "The credentials you entered are invalid.");
            }

            return View(login);
        }

        [HttpPost]
        public async Task<IActionResult> Register(RegisterUserDTO dto)
        {
            if (ModelState.IsValid)
            {
                User appUser = MapToUserModel(dto);

                IdentityResult result = await userManager.CreateAsync(appUser, dto.Password);

                if (result.Succeeded)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    AttachUserCreationErrors(result);
                }
            }

            return View(dto);
        }

        private User MapToUserModel(RegisterUserDTO user)
        {
            return new User
            {
                UserName = user.Name,
                Email = user.Email
            };
        }

        private void AttachUserCreationErrors(IdentityResult result)
        {
            foreach (IdentityError error in result.Errors)
                ModelState.AddModelError("", error.Description);
        }
    }
}
