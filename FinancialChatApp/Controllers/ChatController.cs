﻿using FinancialChatApp.Common;
using FinancialChatApp.Core;
using FinancialChatApp.Core.DTO;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace FinancialChatApp.Controllers
{
    [Authorize]
    public class ChatController : Controller
    {
        private readonly IUnitOfWork unitOfWork;

        public ChatController(IUnitOfWork dataAcessService)
        {
            this.unitOfWork = dataAcessService;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult GetLastMessages()
        {
            var messages = PrepareMessageDTOs();

            return Json(messages);
        }

        private List<MessageDTO> PrepareMessageDTOs()
        {
            return unitOfWork.MessageRepository.GetLastMessages()
                .Select(m => new MessageDTO
                {
                    User = m.User.UserName,
                    Body = m.Body,
                    Timestamp = ServerDateBuilder.GetServerDateTime(m.Timestamp)
                }).ToList();
        }
    }
}
