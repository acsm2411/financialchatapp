﻿namespace FinancialChatApp.Core.Interfaces
{
    public interface IRabbitMQService
    {
        void Connect();
        void SendMessage(string message);
    }
}
