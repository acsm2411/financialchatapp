﻿using FinancialChatApp.Core.Domain;
using System.Collections.Generic;

namespace FinancialChatApp.Core.Repositories
{
    public interface IMessageRepository : IRepository<Message>
    {
        List<Message> GetLastMessages();
    }
}
