﻿using System;

namespace FinancialChatApp.Core.DTO
{
    [Serializable]
    public class MessageDTO
    {
        public string Body { get; set; }
        public string User { get; set; }
        public string Timestamp { get; set; }
    }
}
