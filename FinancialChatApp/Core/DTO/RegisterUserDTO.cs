﻿using System;
using System.ComponentModel.DataAnnotations;

namespace FinancialChatApp.Core.DTO
{
    [Serializable]
    public class RegisterUserDTO
    {
        [Required]
        public string Name { get; set; }
        [Required]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "Invalid email format")]
        public string Email { get; set; }
        [Required]
        public string Password { get; set; }
    }
}
