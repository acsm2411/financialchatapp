﻿using System;
using System.ComponentModel.DataAnnotations;

namespace FinancialChatApp.Core.DTO
{
    [Serializable]
    public class LoginDTO
    {
        [Required]
        public string Email { get; set; }
        [Required]
        public string Password { get; set; }
    }
}
