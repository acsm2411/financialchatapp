﻿using FinancialChatApp.Core.Repositories;
using System;

namespace FinancialChatApp.Core
{
    public interface IUnitOfWork : IDisposable
    {
        public IMessageRepository MessageRepository { get; }

        int Complete();
    }
}
