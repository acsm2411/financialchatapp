using FinancialChatApp.Core;
using FinancialChatApp.Core.Domain;
using FinancialChatApp.Core.Interfaces;
using FinancialChatApp.Core.Repositories;
using FinancialChatApp.Hubs;
using FinancialChatApp.Persistance;
using FinancialChatApp.Persistance.Repositories;
using FinancialChatApp.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;

namespace FinancialChatApp
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public IConfiguration Configuration { get; set; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc()
                .AddRazorRuntimeCompilation();
            services.AddSignalR();
            services.AddDbContext<FinancialChatAppDbContext>(options => options.UseSqlServer(Configuration["ConnectionStrings:MainConnection"]));
            services.AddIdentity<User, IdentityRole>(config =>
                {
                    config.Password.RequiredLength = 5;
                    config.Password.RequireDigit = false;
                    config.Password.RequireUppercase = false;
                    config.Password.RequireNonAlphanumeric = false;
                    config.Password.RequireLowercase = false;
                    config.User.AllowedUserNameCharacters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._ ";
                    config.User.RequireUniqueEmail = true;
                })
                .AddEntityFrameworkStores<FinancialChatAppDbContext>()
                .AddDefaultTokenProviders();

            services.ConfigureApplicationCookie(options =>
            {
                options.LoginPath = "/Login/Index";
            });

            services.AddSingleton<IRabbitMQService, RabbitMQService>();
            services.AddScoped<IMessageRepository, MessageRepository>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app,
            IWebHostEnvironment env,
            IHostApplicationLifetime lifetime,
            FinancialChatAppDbContext context)
        {

            Console.WriteLine("Creating FinancialChatApp database (if needed)");
            context.Database.Migrate();
            Console.WriteLine("Database creation step finished");

            app.UseRouting();
            app.UseStaticFiles();
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapControllerRoute("default", "{controller=Login}/{action=Index}/{id?}");
                endpoints.MapHub<ChatHub>("/ChatHub");
            });

            lifetime.ApplicationStarted.Register(() => RegisterRabbitMQ(app.ApplicationServices));
        }

        private void RegisterRabbitMQ(IServiceProvider serviceProvider)
        {
            var rabbitMQService = (IRabbitMQService)serviceProvider.GetService(typeof(IRabbitMQService));
            rabbitMQService.Connect();
        }
    }
}
