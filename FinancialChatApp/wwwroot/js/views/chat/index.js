﻿window.onload = () => {
    const connection = new signalR.HubConnectionBuilder().withUrl("/ChatHub").build();
    const sendButton = document.getElementById("send");
    const parentElement = document.getElementById("message-board");
    const chatBox = document.getElementById("chatBox")
    let localMessageQueue = [];

    chatBox.onkeyup = (event) => {
        if (event.keyCode === 13) {
            sendButton.click();
        }
    }; 

    sendButton.disabled = true;

    connection.on("ReceiveMessage", function (user, message, timestamp)
    {
        addToLocalMessageQueue(user, message, timestamp);
        appendNewMessage(user, message, timestamp);
        parentElement.scrollIntoView(false);
    });

    loadMessagesFromServerAndStartConnection();

    sendButton.onclick = () =>
    {
        let message = chatBox.value.trim();

        if (message.length > 0)
        {
            connection.invoke("SendMessage", user, message).catch(function ()
            {
                appendNewMessage(BOT_NAME, 'There is an error in the connection, please refresh and try again',
                    getClientDateTime())
            });

            if (message.startsWith('/stock='))
            {
                if (message.length >= 8)
                {
                    let stockCode = message.substring(7);

                    connection.invoke("SendExecutionSignalForFinancialBot", stockCode).catch(function (err)
                    {
                        let error = err.toString().substring(error.indexOf("HubException:") + 13);

                        appendNewMessage(BOT_NAME, error, getClientDateTime())
                    });
                }
                else {
                    connection.invoke("SendMessage", BOT_NAME, "stock code required").catch(function () {
                        appendNewMessage(BOT_NAME, 'There is an error in the connection, please refresh and try again',
                            getClientDateTime())
                    });
                }
            }

            chatBox.value = '';
        }
    }

    function appendNewMessage(user, message, timestamp)
    {
        let msg = message.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;");
        let encodedMsg = `<span class="font-weight-bold">${timestamp} - ${user} says:</span> ${msg}`;
        let li = document.createElement("li");
        li.innerHTML = encodedMsg;
        document.getElementById("message-board").appendChild(li);
    }

    function getClientDateTime()
    {
        return new Date().toLocaleString("en-GB", { hour12: true}).replace(',',''); 
    }

    function loadMessagesFromServerAndStartConnection()
    {
        $.ajax({
            url: "/Chat/GetLastMessages",
            contentType: "application/json",
            method: "GET",
            success: function (data)
            {
                localMessageQueue = data;

                if (localMessageQueue.length > 0)
                {
                    localMessageQueue.forEach((message) =>
                    {
                        appendNewMessage(message.user, message.body, message.timestamp);
                    });
                }
            }
        }).then(() =>
        {
            connection.start().then(function () {
                sendButton.disabled = false;
            })
                .catch(function (err) {
                    return console.error(err.toString());
                });
        });
    }

    function addToLocalMessageQueue(user, message, timestamp)
    {
        let messageObject =
        {
            body: message,
            timestamp: timestamp,
            user: user
        }

        localMessageQueue.push(messageObject)

        adjustFrontendListIfNeeded();
    }

    function adjustFrontendListIfNeeded()
    {
        if (localMessageQueue.length == 51)
        {
            parentElement.firstChild.remove()
            localMessageQueue.shift();
        }
    }
};


