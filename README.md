**Prerequisites:**

- Erlang
- RabbitMQ Server
- Sql Server

**Installation and Configuration Steps:**

1. Install the needed programs (**Prerequisites** folder inside the project folder), you will find the installers for 2 of those in the zip file.

    - Erlang -> otp_win64_23.1.exe
    - RabbitMQ -> rabbitmq-server-3.8.8.exe
2. The application uses Windows Authentication, this must be enabled in SQL Server. Also the server name used in the connection string is just localhost (in case you're using an SQL Server Express instance and your server name is localhost/EXPRESS).
3. Install the application using FinanceChatAppInstaller.msi

**Usage:**

After the app installation is finished you should find 2 shorcuts in your desktop

    - FinancialChatApp
    - FinancialChatBot

To start the main app you must run FinancialChatApp, this will create the database needed (if it doesn't exist) and then will run in http://[your_private_ip]:5000 or http://localhost:5000, the app should be able to execute without the Bot and you should be able to send messages with the other users, the only issue should be that you won't receive any answer when using the **/stock** command. When you start the Bot (FinancialChatBot) the queue in the RabbitMQ Server should start executing and sending back the response if you executed the **/stock** command without starting the Bot. The Bot runs in http://[your_private_ip]:5002 or http://localhost:5002.

**Important:** the **/stock** command was configured to be case sensitve so if you input /Stock=[stock-code] or /STOCK=[stock-code] those ones will be considered as normal messages.

**Extra tips:**

1. You can use the **Return/Enter** key to send a message (if the chatBox is not empty)
2. The IP used to create the server is your private IP, this was done so you can access the application remotely. If no private IP is found then the application will use **localhost**





