using FinancialChatBot.Services;
using NUnit.Framework;

namespace FinancialChatBot.Tests
{
    public class FileDownloadServiceTests
    {
        private FileDownloadService fileDownloadService;

        [SetUp]
        public void Setup(FileDownloadService fileDownloadService)
        {
            this.fileDownloadService = fileDownloadService;
        }

        [TestCase("1")]
        [TestCase("hola")]
        [TestCase("notastock")]
        [TestCase("whatever")]
        public void GetStockQuoteFromWeb_WrongOrNotExistentStockCode_ThrowsException(string value)
        {
            Assert.That(() => fileDownloadService.GetStockQuoteFromWeb(value), Throws.Exception);
        }

        [TestCase("aapl.us")]
        [TestCase("msft.us")]
        public void GetStockQuoteFromWeb_RightStockCode_ReturnValueIsNotEqualsToZero(string value)
        {
            var result = fileDownloadService.GetStockQuoteFromWeb(value);

            Assert.That(result, Is.Not.EqualTo(0));
        }
    }
}